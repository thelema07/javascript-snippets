import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BlogComponent } from "../blog.component";

// const routes: Routes = [
//   { path: 'customer-list',
//     loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule) }
//   ];

const routes: Routes = [
  {
    path: "",
    component: BlogComponent
  },
  { path: "", redirectTo: "", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule {}
