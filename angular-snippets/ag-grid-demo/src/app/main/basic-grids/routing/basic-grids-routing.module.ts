import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BasicGridsComponent } from "../basic-grids.component";

const routes: Routes = [
  {
    path: "",
    component: BasicGridsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicGridsRoutingModule {}
