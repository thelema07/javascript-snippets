import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "basic-grids",
    loadChildren: () =>
      import("./main/basic-grids/basic-grids.module").then(
        m => m.BasicGridsModule
      )
  },
  {
    path: "",
    loadChildren: () =>
      import("./main/home/home.module").then(m => m.HomeModule)
  },
  { path: "", redirectTo: "", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
