import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EcommerceComponent } from "./ecommerce.component";
import { EcommerceMainComponent } from "./ecommerce-main/ecommerce-main.component";
import { EcommerceRoutingModule } from "./_ecommerce-routing/ecommerce-routing.module";

@NgModule({
  imports: [CommonModule, EcommerceRoutingModule],
  declarations: [EcommerceComponent, EcommerceMainComponent]
})
export class EcommerceModule {}
