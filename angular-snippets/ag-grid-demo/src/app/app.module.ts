import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AgGridSetupModule } from "./ag-grid-setup/AgGridSetup.module";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./layout/header/header.component";
import { BootstrapCssSetupModule } from "./bootstrap-css/BootstrapCssSetup.module";

import { HomeModule } from "./main/home/home.module";
import { BasicGridsModule } from "./main/basic-grids/basic-grids.module";
import { AgGridModule } from "ag-grid-angular";

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AgGridSetupModule,
    BootstrapCssSetupModule,
    HomeModule,
    BasicGridsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
