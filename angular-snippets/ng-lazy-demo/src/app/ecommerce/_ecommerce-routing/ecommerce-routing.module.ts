import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EcommerceMainComponent } from "../ecommerce-main/ecommerce-main.component";

// const routes: Routes = [
//   { path: 'customer-list',
//     loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule) }
//   ];

const routes: Routes = [
  {
    path: "",
    component: EcommerceMainComponent
  },
  { path: "", redirectTo: "", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EcommerceRoutingModule {}
