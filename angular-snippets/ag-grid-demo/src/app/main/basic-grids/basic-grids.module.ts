import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BasicGridsComponent } from "./basic-grids.component";
import { BasicGridsRoutingModule } from "./routing/basic-grids-routing.module";
import { AgGridModule } from "ag-grid-angular";

@NgModule({
  imports: [
    CommonModule,
    BasicGridsRoutingModule,
    AgGridModule.withComponents([BasicGridsComponent])
  ],
  declarations: [BasicGridsComponent]
})
export class BasicGridsModule {}
