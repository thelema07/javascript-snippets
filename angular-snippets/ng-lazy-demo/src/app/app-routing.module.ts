import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { BlogModule } from "./blog/blog.module";

// const routes: Routes = [
//   { path: 'customer-list',
//     loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule) }
//   ];

const routes: Routes = [
  {
    path: "ecommerce",
    loadChildren: "src/app/ecommerce/ecommerce.module#EcommerceModule"
  },
  {
    path: "blog",
    loadChildren: "src/app/blog/blog.module#BlogModule"
  },
  { path: "", redirectTo: "", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
