import { Component, OnInit } from "@angular/core";
import * as grid from "ag-grid-angular";

@Component({
  selector: "app-basic-grids",
  templateUrl: "./basic-grids.component.html",
  styleUrls: ["./basic-grids.component.css"]
})
export class BasicGridsComponent implements OnInit {
  columnDefs = [
    { field: "Customer Id" },
    { field: "Type of Survey" },
    { field: "Survey Title" },
    { field: "Name" },
    { field: "Sector" },
    { field: "Manager" }
  ];

  rowData = [
    {
      "Customer Id": "Toyota",
      "Type of Survey": "Celica",
      "Survey Title": 35000,
      Name: "Axxis Man",
      Sector: "Management",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Ford",
      "Type of Survey": "Mondeo",
      "Survey Title": 32000,
      Name: "Soviet Man",
      Sector: "Investment",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Porsche",
      "Type of Survey": "Boxter",
      "Survey Title": 72000,
      Name: "Cinese Man",
      Sector: "Gamble",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Toyota",
      "Type of Survey": "Celica",
      "Survey Title": 35000,
      Name: "Axxis Man",
      Sector: "Management",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Ford",
      "Type of Survey": "Mondeo",
      "Survey Title": 32000,
      Name: "Soviet Man",
      Sector: "Investment",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Porsche",
      "Type of Survey": "Boxter",
      "Survey Title": 72000,
      Name: "Cinese Man",
      Sector: "Gamble",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Toyota",
      "Type of Survey": "Celica",
      "Survey Title": 35000,
      Name: "Axxis Man",
      Sector: "Management",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Ford",
      "Type of Survey": "Mondeo",
      "Survey Title": 32000,
      Name: "Soviet Man",
      Sector: "Investment",
      Manager: "Capitan Amalillo"
    },
    {
      "Customer Id": "Porsche",
      "Type of Survey": "Boxter",
      "Survey Title": 72000,
      Name: "Cinese Man",
      Sector: "Gamble",
      Manager: "Capitan Amalillo"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
