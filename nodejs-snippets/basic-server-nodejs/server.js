const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.json({ msg: "This is a simple server!!!" });
});

app.listen(4001, () => console.log("Server running!!"));
