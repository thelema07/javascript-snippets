import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";

@NgModule({
  imports: [CommonModule, BrowserAnimationsModule, BsDropdownModule.forRoot()],
  exports: [BrowserAnimationsModule, BsDropdownModule]
})
export class BootstrapCssSetupModule {}
