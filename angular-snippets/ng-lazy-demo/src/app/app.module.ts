import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { BlogModule } from "./blog/blog.module";
import { EcommerceModule } from "./ecommerce/ecommerce.module";
import { AppRoutingModule } from "./app-routing.module";

import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BlogModule, EcommerceModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
