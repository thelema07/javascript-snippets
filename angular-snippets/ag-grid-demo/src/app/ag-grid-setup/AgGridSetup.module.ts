import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AgGridModule } from "ag-grid-angular";
import { BasicGridsComponent } from "../main/basic-grids/basic-grids.component";

@NgModule({
  imports: [CommonModule, AgGridModule.withComponents([BasicGridsComponent])]
})
export class AgGridSetupModule {}
